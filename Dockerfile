FROM openjdk:11-jdk
COPY ./*.jar /srv/application.jar
EXPOSE 8080
CMD ["java", "-jar", "/srv/application.jar"]
